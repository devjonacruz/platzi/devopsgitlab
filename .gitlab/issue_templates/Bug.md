Summary - Resumen

(Da el resumen del issue)

Steps to reproduce - Pasos para reproducir

(Indica los pasos para reproducir el bug)

What is the current behavior? - Cual es el comportamiento actual

What is the expected behavior? - Cual es el comportamiento esperado