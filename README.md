# INTRODUCCIÓN A GITLAB

Gitlab es una compañía que realiza un proyecto open source, un producto especializado en el ciclo de vidas del DevOps.

* Administración: Nos da opciones de autenticación, autorización, analytics y self hosted gitlab.
* Planificación: Nos da issues, milestones, burndown charts, discusiones, boards, service desk, to-dos, etc.
* Creación: Generar proyectos, grupos, repositorios de código, merge request, integración y automatización.
* Verificación: Correr pruebas automatizadas, calidad del código, review apps y code coverage.
* Empaquetación: Container registry, paquetes privados.
* Distribución(release): Deployment strategies, ambientes, Gitlab pages, feature flags.
* Configuración: Existe la posibilidad de que debas cambiar la configuración de tu sistema. Auto DevOps, integración con Kubernetes, Knative serverless, manejo de secreto, chatops.
* Monitoreo: Prometheus, Jaeger, Sentry.
* Seguridad: Container scanning, dependency scanning, dynamic security testing, static security testing, manejo de licencias, security dashboard.
* Defensa: Gitlab está trabajando en herramientas para esta etapa como Firewalls, threat detection, data loss prevention y más.

# Gitlab vs Github

Github y Gitlab son plataformas que comparten algunas funcionalidades aunque tienen objetivos diferentes.

Github nace como un repositorio en la nube colaborativo basado en Git para permitir participar en proyectos. En Github una persona fuera de una organización puedan colaborar en estos proyectos, es como una red social de programadores. Fue adquirido por Microsoft.

Gitlab nació como una versión open source de Github y a lo largo del tiempo ha encontrado un nicho en agregar herramientas y generar integraciones directamente al producto. Tiene una visión de que su cliente principal es alguien especializado en DevOps

# GItlab te ofrece varias formas de autenticación tales como:

* Username y Password
* Two factor authentication(2FA)
* SSH Key `ssh-keygen -t rsa -b 4096 -C "example@email.com"`

# Grupos

Los grupos te permiten compartir recursos entre varios miembros del equipo y organizar la forma en la que trabajas.

* Agrupar proyectos: dónde va a vivir nuestro código y los recursos asociados.
* Otorgar permisos de acceso: qué usuarios de qué equipo van a poder acceder a los recursos de la compañía.
* Compartir recursos: Si tienes cluster de Kubernetes, templates o runners para correr el CI lo puedes compartir entre varios grupos.

Los grupos se utilizan en Gitlab a través de los Namespaces que nos dan una url única para nuestro usuario, grupo y subgrupo.

Visibility

* private
* internal
* public

# Autorización

Existen diferentes formas de autorizar un usuario dentro de un grupo y estos mismos modelos se utilizan para los proyectos:

* Guest: Es Read-only, solo tiene permisos de lecturas. No puede modificar código, crear o comentar issues.
* Reporter: Solo puede crear y comentar en los issues. No puede añadir código.
* Developer: Puede añadir código, también da acceso a los Pipelines de CI, branchs y más, pero no da la capacidad de agregar nuevos miembros.
* Owner / Maintainer: Eres Owner cuando creas un proyecto y Maintainer cuando alguien más te da permisos para administrar ese proyecto.

# Auditoría

Gitlab nos permite generar rastro de auditoría cuando se realizan cambios que pueden modificar el grupo, el proyecto o la instancia.

# Proyectos

Los proyectos tienen tres componentes fundamentales:

* Issue tracker: No es uno genérico para cualquier compañía, es específico para equipos que desarrollan software y adentro encontrarás conceptos como milestone, estimados de tiempo y más.
* Code repository: Es el lugar centrar que nos va a permitir compartir y colaborar alrededor del código.
* Gitlab CI: Nos muestra la posibilidad de automatización con la inclusión de Continuous Integration.

# Tipos de desarrollo

Las principales diferencias entre Agile y Waterfall es que en el primero encontramos un proceso iterativo y en el segundo utilizamos un proceso previamente definido. En Agile estamos realizando sprints, pequeños esfuerzos de trabajo para al final tener un entregable y mandarlo a producción.

En Waterfall tenemos nuestro entregable hasta el final del proyecto, como crear una casa de manera modular.

Waterfall - Fases

* Define
* Build
* Test - especificciones se encuentren ahi
* Release

Agile - ciclos

* Define
* Test
* Build
* Release

# Planificación en Gitlab-Issues

Los issues son el punto donde inicia una conversación sobre el código.

Los issues permiten:

* Discutir la implementación de una nueva idea.
* Sugerir propuestas de features.
* Hacer preguntas.
* Reportar bugs y fallos.
* Obtener soporte.
* Planear nuevas implementaciones de código.

Puedes añadir templates a tus issues para poder estandarizar la forma en la que se abren, podemos incluirlo de cualquier tipo. Debes crear un archivo o una estructura de carpeta como:

`.gitlab/issue_templates/Bug.md`

# Planificación en Gitlab-Etiquetas

El siguiente paso es clasificar los issues basados en etiquetas, suele salirse de control la forma en la que se reportan y una manera sencilla de organizar es con etiquetas.

Las etiquetas nos permiten:

* Categorizar issues o merge request con títulos descriptivos.
* Filtrar y buscar en Gitlab
* Seguir temas a través de notificaciones.


# Planificación en Gitlab-Pesos

* El primer punto que debes tener en cuenta es que la estimación es un trabajo de equipo. Es importante que diversos equipos de trabajo colaboren en la estimación. Los Desarrolladores, Diseñadores, Product Managers, etc. tienen diversas perspectivas sobre lo que implica desarrollar una nueva funcionalidad. Cuando la estimación se realiza tomando en cuenta estas perspectivas existe una mayor probabilidad de que la estimación se acerque a la realidad.

* Otro punto importante es que las estimaciones funcionan mejor cuando son relativas. Es decir es mejor encontrar un trabajo relativamente sencillo en el que todo el equipo se encuentre de acuerdo y estimar a partir de ese punto. Por ejemplo, si todo el equipo está de acuerdo en que añadir verificación a los campos de un formulario es un 2, entonces estimemos con base en ese acuerdo.

* Es importante recordar que cuando estimamos, es buena práctica tener un sistema de estimación en el que el equipo esté de acuerdo. Un ejemplo muy usado en la industria son los puntos Fibonacci. Es decir, se utiliza la secuencia de Fibonacci para asignar pesos a issues (1, 2, 3, 5, 8, 13, etc.). Otra forma, es utilizar tallas de camisas (S, M, L, XL, XXL, etc.). En todo caso, lo importante es que el equipo entienda estos sistemas y los adopte en sus prácticas.

* Por último, cuando incluímos a varios miembros del equipo en la estimación es importante que sus opiniones no se vean sesgadas por el resto de sus compañeros. Por eso, una práctica que me gusta mucho es la de jugar “Estimation Poker”. En esta modalidad de estimación, todo el equipo tiene barajas que representan los puntos y cuando se pone a discusión un issue, los miembros del equipo revelan su estimado con las barajas. Si todos están de acuerdo, perfecto. Pero si existen grandes discrepancias es momento de escuchar y de volver a evaluar con la nueva información que nos ha sido proporcionada. En todo caso, lo importante es mejorar con el paso de los sprints y que las estimaciones, quizá nunca perfectas, sean lo más realistas posibles.

# Planificación en Gitlab-Milestones

Los Milestones permiten agrupar issues para completarlos en un tiempo determinado.

* Milestone como agile sprint
    * Sprint 5
* Milestone como release
    * V1.1.2

Brundown chart nos permite determinar qué tan avazando vamos dentro de un sprint y nos permite tomar acciones cuando todavía es relevante.

# Planificación en Gitlab-Boards

Los boards son una forma de visualizar los flujos de trabajo, de ver quién está trabajando en qué issues y son unas de las herramientas más importantes que existen dentro de Gitlab.

* Se puede utilizar para Kanban o Scrum.
* Une los mundos de los issue tracking y Project managment.

# Planificación en Gitlab-Service Desk

El Service Desk es la capacidad que te da Gitlab de abrir issues a través de correo electrónico.

* Permite dar soporte a través de email a tus clientes directamente desde Gitlab.
* Permite que el equipo no técnico reporte bugs o abra issue sin necesidad de que tengan una cuenta de Gitlab.
* Cuando se activa el servicio, se genera un email único para el proyecto.

# Planificación en Gitlab-Quick actions

Las Quick Actions son atajos textuales para acciones comunes en issues, epics, merge request y commits que normalmente son ejecutadas a través de la UI de Gitlab. Los comandos se pueden agregar al momento de crear un issue o un merge request o en los comentarios de los mismos. Cada comando debe incluirse en una línea separada para que se detecten y ejecuten. Una vez ejecutados, los comandos se retiran del texto y no pueden verse en el comentario o descripción del issue.

https://docs.gitlab.com/ee/user/project/quick_actions.html
    
# Merge requests

Los Merge Requests son la puerta de entrada a nuestro código, es el momento en donde definimos que un cambio sugerido por otra persona será unido a nuestra rama master o rama principal. Para tomar esta decisión se necesita mucha información: si los cambios fueron correctos, resuelven el issue, si surgen problemas de seguridad, si mejora nuestro performance.

El título del merge requests tiene prefijado WIP que significa Work in Progress

# Continuous Integration-CI

El Continuous Integration es una práctica en la que los desarrolladores envían sus cambios a un repositorio central, lo cual detona builds y pruebas automatizadas.

* Ayuda a encontrar bugs de manera oportuna
* Aumenta la velocidad de los releases
* Automatiza el pipeline que lleva código desde la computadora del desarrollador hasta el dispositivo del cliente.

Conceptos similares CI

* Continuos Integration
* Continuos Delivery
* Continuos Deployment

# Gitlab CI

Gitlab CI es el hub central de automatización de Gitlab, es el pedazo que podemos configurar libremente para generar las automatizaciones necesarias para que nuestro flujo de trabajo requiera poca o ninguna interacción humana.

* Continuamente construye, prueba y despliega cambios pequeños al código.
* Se configura con el archivo gitlab-ci.yml

También nos permite realizar Continuous Delivery y Continuous Deployment.

Continuos Delivery
* Artefacto listo para enviar a producción

Contunous Deployment
* Continuo envio de codigo a producción

Workflow
* New Branch
* Commits
* Push your changes
* Build and test - Continous Integration
* Preview with Review Apps - Deploy to a dynamic environment
* Review
* Approve
* Merge
* Deploy - Continuos Deployment

Variables
* Trigger variables
* Project variables y protected variables
* Group varialbles
* YAML job level variables
* YAML global variables
* Deployment variables
* Pre defined variables

`gitlab-ci.yml` -  funciona a traves de keywords

```` 
image: node:11.1.0

stages: - pasos por los que va a pasar
-build
-test
-deploy

compile: - jobs
    stage: build - pertenece este job al stage build
    variables:
        FOO: bar
    script: -  aqui es donde automatizamos
        - ng build
    only: - only o except - cuando corre este job
        - branches
````

El archivo `.gitlab-ci.yml` sirve para configurar el comportamiento de Gitlab CI en cada proyecto. En el archivo define la estructura y el orden de los pipelines y determina qué ejecutar con el Gitlab runner y qué decisiones tomar cuando condiciones específicas se cumplen (como cuando un proceso falla o termina exitosamente).

El archivo tiene muchas opciones de configuración, pero aquí nos vamos a enfocar en tres: image, stages y jobs.

La primera opción de configuración es image. image nos sirve para determinar qué imagen de Docker vamos a utilizar para ejecutar el runner. Hay que recordar que, en su nivel más básico, los trabajos de CI son simplemente automatización de scripts. Con esto en mente, tenemos que determinar qué ambiente necesita nuestro script para correr de manera exitosa. ¿Necesitas instalar dependencias desde NPM y ejecutar scripts de package.json? Entonces es muy probable que la imagen de Node te sirva como base. Quizá necesitas correr pruebas unitarias de una aplicación de Python e instalar dependencias desde PyPi; entonces deberías instalar la imagen de Python.

Al final del día, el keyword image nos permite “preinstalar” los paquetes que nuestro script necesitará para correr: desde sistema operativo y lenguajes de programación, hasta software específico como bases de datos.

Un último punto, image puede utilizarse de manera global o por cada job que ejecutemos. Es decir, si nuestro proyecto lo requiere podemos utilizar una imagen de Node para un job y otra de Ruby para otro.

````
# gitlab-ci.yml
image: node:11.1.0
````

````
# ó
job1:
  image: python:3.7
````

Por su parte, los stages nos permiten definir las etapas que atravesará nuestro pipeline cuando corra. Cada stage (etapa) puede contener uno o más jobs que correrán en paralelo. Los stages permiten agrupar los jobs que pertenezcan a una categoría y permiten crear diversas dependencias entre cada job. Un ejemplo de un pipeline multietapa sería el siguiente:

Install -> Test -> Build -> Deploy

Dicho pipeline lo podríamos configurar en .gitlab-ci.yml de la siguiente manera:

````
# gitlab-ci.yml

stages:
  - install
  - test
  - build
  - deploy

job1:
  stage: install
…
````

Es importante recordar que para configurar nuestros pipelines de manera correcta, tenemos que declarar el nombre de nuestras etapas como una lista bajo el keyword stages y luego indicar en cada job a qué etapa pertenece con el keyword stage.

Por último, los jobs son los encargados de ejecutar los scripts de automatización en cada etapa. En este sentido, un job puede tener casi cualquier nombre (aunque siempre debes intentar nombrar de acuerdo a la función de lo que estás nombrando) y debe contener un script que se ejecutará. Los jobs se ejecutan por los runners cuando se encuentran disponibles. Gran parte de la configuración adicional de los jobs está relacionada con las condiciones sobre las cuales se debe ejecutar y que artefactos exporta para otros jobs en el pipeline.

````
# gitlab-ci.yml

job1:
  script:
    - echo “Hello, world”
    - npm install
    - echo “etc.”
…
````

Recuerda que el archivo `.gitlab-ci.yml` es fundamental para configurar nuestro CI, pero nuestro IDE no posee las reglas para determinar si su estructura es válida. De hecho, si tienes el tiempo, crear un plugin de VS Code o de VIM para estos efectos sería una gran idea. Por esto, es importante validar nuestra configuración con la herramienta de linting de Gitlab. La puedes encontrar en cada uno de tus proyectos si configuras la siguiente URL con tus datos.

# Implementando Gitlab pages

Gitlab Pages nos otorga ciertas funcionalidades como:

* Hosting estático para servir nuestro website.
* Integración con Gitlab CI.
* Dominios personalizados.

# ¿Qué es el Desarrollo Ágil?

El Desarrollo ágil de software viene del concepto básico de agilidad, la capacidad de responder a cambios.

En el “Agile Manifest” la biblia del desarrollo ágil eligen el término por su adaptabilidad.

Pero… ¿qué es el desarrollo ágil?
El desarrollo ágil abarca todo un set de frameworks y prácticas basadas en los 12 principios del manifiesto ágil, en los que tú eliges cuáles de estos principios son los mejores para tu desarrollo.

Los cuatro valores del desarrollo Agile:

* A los individuos y su interacción, por encima de los procesos y las herramientas.
* El software que funciona, por encima de la documentación exhaustiva.
* La colaboración con el cliente, por encima de la negociación contractual.
* La respuesta al cambio, por encima del seguimiento de un plan.

Según la Wikipedia los 12 principios se resúmen así:

* Nuestra principal prioridad es satisfacer al cliente a través de la entrega temprana y continua de software de valor.
* Son bienvenidos los requisitos cambiantes, incluso si llegan tarde al desarrollo. Los procesos ágiles se doblegan al cambio       como ventaja competitiva para el cliente.
* Entregar con frecuencia software que funcione, en periodos de un par de semanas hasta un par de meses, con preferencia en los     periodos breves.
* Las personas del negocio y los desarrolladores deben trabajar juntos de forma cotidiana a través del proyecto.
* Construcción de proyectos en torno a individuos motivados, dándoles la oportunidad y el respaldo que necesitan y procurándoles    confianza para que realicen la tarea.
* La forma más eficiente y efectiva de comunicar información de ida y vuelta dentro de un equipo de desarrollo es mediante la       conversación cara a cara.
* El software que funciona es la principal medida del progreso.
* Los procesos ágiles promueven el desarrollo sostenido. Los patrocinadores, desarrolladores y usuarios deben mantener un ritmo     constante de forma indefinida.
* La atención continua a la excelencia técnica enaltece la agilidad.
* La simplicidad como arte de maximizar la cantidad de trabajo que se hace, es esencial.
* Las mejores arquitecturas, requisitos y diseños emergen de equipos que se autoorganizan.
* En intervalos regulares, el equipo reflexiona sobre la forma de ser más efectivo y ajusta su conducta en consecuencia.

# Gitlab autodevops

Gitlab autodevops es una solución que te permite generar un flujo de devops inmediato con la creación del proyecto que incluye todas las mejores prácticas.

Features:

* Auto build
* Auto test
* Auto Code Quality
* Auto SAST
* Auto dependency scanning
* Auto container scanning
* Auto review apps
* Auto Dast
* Auto Deploy
* Auto performance
* Auto testing

Prerequisitos

* Gitlab Runner
* Kubernetes
* Base domain
* Prometheus.

Personalización

* Dockerfile
* Variables

````
...
include:
    template: Auto-Devops.gitlab-ci.yml
...
````

# Gitlab container registry

Gitlab container registry permite almacenar imágenes de Docker para uso posterior. En un caso tradicional, cada vez que el CI tiene un build exitoso, una nueva imagen se envía al container registry

# Introducción a DevSecOps

En el pasado, el equipo de Seguridad actuaba aislado y actuaba únicamente al final del proceso de desarrollo, un flujo de waterfall. Esto funcionaba porque eran ciclos de desarrollo que llevaban meses o años.

DevSecOps significa pensar en la seguridad de la aplicación a lo largo del proceso, desde el principio. Se trata de automatizar la seguridad e incluirla en el ciclo de vida de la aplicación (no más seguridad externa y en perímetros)

# Firmas de seguridad

GPG permite identificar, sin lugar a dudas, de quién proviene un commit; añade una capa adicional de seguridad a Git para prevenir ““caballos de troya””.

Gitlab despliega un banner junto a los commits para mostrar que dichos commits están verificados.

`gpg --full-gen-key`

`gpg --list-secret-keys --keyid-format LONG`

<your_email>

Exportarla

`gpg --armor --export key_id`

Configura git

`git config --global user.signingkey key_id`
`git config --global gpg.program gpg`

# Pruebas estáticas de seguridad

Las pruebas estáticas de seguridad analizan nuestros archivos buscando patrones inseguros de código.

* Crean un reporte que es añadido como widget al merque request
* Utilizan la imagen de Docker SAST de Gitlab

## Tipos de vulnerabilidades

* Critical: Existe un falla de código que da acceso de root o a los sistemas sin necesidad de ingeniería social. Debes atenderla de inmediato.
* High: Si se explota este tipo de vulnerabilidad estamos en riesgo de perder datos. Es difícil de explotar.
* Medium: El hacker va a tener que realizar trabajo adicional para obtener el acceso deseado.
* Low: No representan un riesgo de pérdida de datos.
* Unknow: No han sido clasificadas todavía y debes evaluarlas una por una.

## SAST

* Corre analisis estativo en el codigo
* Verifica que no existan problemas potenciales de seguridad en el codigo de la aplicación
* Verifica que no existen secretos "hardcodeados"
* Crea un reporte que es añadido como widget al merge request
* Utiliza la imagen de Docker SAST de Gitlab

`gitlab-ci.yml`

````
...
include:
    template: SAST.gitlab-ci.yml
...
````

# Escaneo de contenedores

Container Scanning

    Utiliza Clari y clair-scanner para verificar los contenedores
    Si deseas omitir vulnerabilidades, las puedes incluir en el archivo clair-whitelist.yml
    Verifica que los paquetes instalados a nivel contenedor no tengan vulnerabilidades de seguridad

`gitlab-ci.yml`

````
...
include:
    template: Container-Scanning.gitlab-ci.yml
...
````

# Escaneo de dependencias

El Dependency scanning analiza estáticamente las dependencias del proyecto para encontrar vulnerabilidades. Puede generar un reporte que se añade al merge request y utiliza la imagen de Docker Dependency Scanning de Gitlab

`gitlab-ci.yml`

````
...
include:
    template: Dependency-Scanning.gitlab-ci.yml
...
````

# Pruebas dinámicas de seguridad

Las pruebas dinámicas de seguridad asumen que es un atacante externo y la aplicación es un blackbox para así correrle ciertas pruebas.

* Utiliza OWASP ZAP proxy y ZAP baseline.
* Corre análisis pasivo.
* Genera un reporte que puede ser verificado en el merge request.

## DAST

* Corre escaneos de seguridad en una aplicación que ya está en runtime
* Utiliza OWASP ZAP proxy y ZAP baseline
* Corre analisis pasivo
* Genera un reporte que puede ser verificado en el merge request

`gitlab-ci.yml`

````
...
include:
    template: DAST.gitlab-ci.yml
...
````

# Gitlab security dashboard

El Gitlab security dashboard es un hub centralizado de información donde tienes visibilidad de las vulnerabilidades que actualmente están corriendo en producción.

Permite acceder rápidamente a los riesgos detectados y aceptados para el ambiente de producción, permite marcar una vulnerabilidad como inválida o no aplicable, genera vínculos a los reportes de seguridad externos para entender mejor una vulnerabilidad.

# Continuous Delivery (CD)

# Ambientes

Los Ambientes se suelen utilizar para determinar si el código escrito cumple con las expectativas del negocio y los requisitos impuestos con antelación para que así las personas puedan aprobarlos o no.

* Permiten realizar pruebas en diferentes ambientes antes de enviar el código a nuestros usuarios.
* Se integran con Gitlab CI para hacer realidad el Continuous Deployment.
* Gitlab lleva el historial de todos los deployments que se han realizado a un ambiente específico.
* Permiten verificar que el Deployment process se encuentre intacto y da la oportunidad de hacer QA

Tenemos algunos tipos como:

* Estáticos
* Dinámicos
* Protegidos

Staggin Producción Desarrollo

````
deploy_production:
    stage: deploy
    environment:
        name: production
        url: https://production.com
    when: manual
````

# Review apps

Las Reviews apps permiten ver los cambios de un feature branch al activar un ambiente para ejecutar el código con cada merge request.

* Los diseñadores y los product managers pueden ver los cambios sin necesidad de levantar un ambiente local en sus computadoras
* Cuando el merge request se aprueba y el feature branch se borra, se detiene el review app y se destruye la infraestructura
* Completamente integrado con GitlabCI y merge request.

````
review:
    stage: review
    script:
        - # script para crear el ambien te y hacer deploy
    environment:
        name: review/$CI_BUILD_REF_NAME
        url: http://$CI_BUILD_REF_SLUG.$APPS_DOMAIN
        on_stop: stop_review
    only:
        - branches
    except:
        - master

stop_review:
    stage: review
    script:
        - #script parra destruir el ambiente
    variables:
        GIT_STRATEGY: none
    when: manual
    environment:
        name: review/$CI_BUILD_REF_NAME
        action: stop
````

# Estrategias de distribución
  
##  Big bang deployment

Como lo sugiere su nombre, los despliegues de Big Bang, actualizan todas las partes del sistema en una sola barrida. Esta estrategia encuentra sus orígenes en los días en que el software se distribuía en medios físicos y el cliente lo instalaba manualmente en su máquina.

Este tipo de despliegues requieren que el negocio ejecute una enorme cantidad de pruebas durante una fase específica del desarrollo, antes de aprobar el despliegue. Este tipo de pruebas, normalmente se asocian con el modelo waterfall en el que el desarrollo se ejecuta en etapas secuenciales.

Las aplicaciones modernas tienen la ventaja de poderse actualizar automáticamente, en el cliente o el servidor, por lo que este tipo de estrategias han sido casi completamente abandonadas por equipos que siguen las metodologías ágiles.

## Rolling deployment

Los rolling deployments, o despliegue en fases, tienen la ventaja de mitigar algunas de las desventajas de los big bang deployments. Esto es así, porque disminuye el riesgo de downtime al desplegar la aplicación a lo largo del tiempo.

Es importante resaltar que el despliegue consiste en reemplazar una versión de la aplicación con otra en fases; de tal manera que existe un tiempo en el que ambas aplicaciones pueden existir. En el caso de un despliegue a Kubernetes, por ejemplo, el reemplazo consiste en destruir el contenedor con la versión anterior y descargar la última versión de la imagen desde el container registry en el cual la alojemos.

Y es aquí donde se alcanza a ver otra ventaja de contenerizar nuestra aplicación: que los rollbacks resultan ser muy sencillos, cuando antes (en el modelo del Big Bang) resultaban imposibles. Hacer rollback es tan sólo destruir de nueva cuenta el pod, y descargar la versión previa (o cualquier otra versión que queramos) desde nuestro container registry.

## Blue Green deployment
   
Esta estrategia, también conocida como A/B deployment, consiste en tener dos ambientes de producción paralelos (uno llamado blue y el otro llamado green) en el cual se despliegan las nuevas versiones de las aplicaciones de manera alternativa. Es decir, si blue tiene instalada la V1 de nuestra aplicación, entonces green tendrá instalada la V2, y cuando se despliegue se la siguiente versión (V3) se utilizará el ambiente blue, nuevamente. ¿Dónde se desplegará el ambiente V4? En green, por supuesto; y la secuencia alternativa continúa indefinidamente.

Una de las ventajas de esta estrategia es que facilita realizar un rollback a la versión anterior, de manera sencilla, cuando nuestra aplicación no se encuentra habilitada para trabajar dentro de contenedores. En caso de que exista una falla en la nueva versión, simplemente se rutea al ambiente previo.

Es importante mencionar, que únicamente el ambiente de la capa de la aplicación se replica. Las bases de datos, al igual que el almacenamiento de archivos binarios (fotos, imágenes, vídeos, por ejemplo), son compartidas por ambos ambientes.

Existe otra modalidad de los despliegues blue green que se conoce como canary deployment. El canary deployment en lugar de rutear todo el tráfico de inmediato, se utiliza una aproximación incremental. Es decir, se comienza a rutear a la nueva versión progresivamente. Por ejemplo, 25% 50% 75% 100% ó 33% 66% 100%, etc.

Una de las ventajas de adoptar esta estrategia es que se puede probar la nueva versión con un subconjunto de los usuarios para determinar si se encuentra estable, y en caso de confirmarse, se rutea todo el tráfico al ambiente green o blue.

## Distribución en Gitlab
   
En Gitlab, cuando utilizamos AutoDevOps, podemos configurar nuestra estrategia de despliegue a través de opciones predeterminadas en el UI o a través de variables de ambiente de Gitlab CI.

Las variables que podemos configurar son las siguientes:
STAGING_ENABLED activa el ambiente staging cuando se le asigna el valor 1.
CANARY_ENABLED activa el ambiente canary cuando se le asigna el valor 1.
INCREMENTAL_ROLLOUT_MODE define la forma en el que el despliegue incremental se realizará. Acepta los valores manual y timed.

# Feature Flags 

Tipos de Feature Flags

## Release Flags
   
La primera categoria se conoce como Release Flags. Este tipo de bandera nos permite implementar una estrategia de Continuous Delivery, dónde los diferentes features se activan de manera manual e independiente. Este tipo de estrategia es importante cuando se requiere lanzar una funcionalidad compleja que requiere estar concluida al 100% para ser lanzada o cuando se necesita coordinar un evento externo junto con el despliegue de la funcionalidad (por ejemplo, cuando el feature se lanza en coordinación con una campaña de marketing).

## Experiment Flags
   
Este tipo de flag se utiliza cuando nuestra aplicación permite la realización de experimentos A/B. Cada usuario de la aplicación es segmentado en cohortes y se muestran diferentes funcionalidades dependiendo del cohorte al que pertenezcan. Este tipo de flags tiene un periodo de vida muy corto, pues una vez que se ha determinado el resultado del experimento, se opta por una u otra versión, y se estandariza el uso en el sistema. También, es importante tener duraciones cortas porque cuando se corren diversos experimentos A/B de manera simultánea, existen altas posibilidades que los experimentos interfieran el uno con el otro, eliminando así la validez estadística del resultado.

## Ops Flags
   
Permiten crear switches que facilitan controlar el comportamiento del sistema en runtime. Existen ocasiones, por ejemplo, en el que los sistemas reciben cargas inusuales y es necesario optimizar los recursos que tenemos disponibles para servir nuestra aplicación. En este caso, a través de feature flags es posible deshabilitar temporalmente servicios no críticos (como quizá un proceso que utiliza mucha memoria o procesamiento), para después habilitarlos una vez que la carga se haya normalizado.

## Permission Flags

Nos permiten habilitar funcionalidades para usuarios específicos de nuestra aplicación. Un ejemplo de lo anterior acontece cuando una compañía decide hacer dogfooding para probar internamente funcionalidades antes de habilitarlas para todos los clientes. En este caso, se puede utilizar una lista de Ids de usuarios para determinar si es necesario mostrar la funcionalidad o no.

# Rollback

Rollback es un mecanismo que nos permite regresar a la versión anterior o donde estés seguro de que la aplicación sigue funcionando. Esto con tan solo un click.

* Gitlab ofrece la funcionalidad de ““re deploy”” para correr cualquier pipeline que haya sido ligado a ambiente.
* Permite automatizar el regreso a ambientes libres de bugs.

# ¿Por qué monitorear?

Cuando el codebase era relativamente estático, operations no se preocupaba mucho del monitoreo, pero ahora con la llegada de DevOps y con cambios muy frecuentes al ambiente, el monitoreo se vuelve indispensable.

## Best practices:

* Monitorea todos los ambientes(incluyendo review apps).
* Familiarizate con las métricas ““normales”” de tu aplicación.
* Automatiza el monitoreo.
* Comparte los datos con el resto de la organización.
    * Reportes.
    * Accesos privilegiados.
* Monitorea aplicación, infraestructura y equipo.

# Métricas de desempeño (performance metrics)

Las métricas de desempeño nos pueden dar una idea de qué tanto está creciendo la infraestructura y la capacidad de respuesta para nuestros clientes.

* Nos ayudan a medir el rendimiento.
* Nos dan una idea de cómo afinar un workload a una query.

* HTTP Error
* Latenciy
* ThrougInput

# Métricas de salud (health metrics)

Las métricas de salud nos permiten entender si nuestros dispositivos o infraestructura están a punto de fallar. Nos ayuda a decidir si debemos aumentar o disminuir nuestros recursos

* Memory
* CPU

# Metricas de equipo

* Cycle analytics.
* Lenguajes de programación utilizados
* Commits por día del mes, de la semana y hora
* Pipelines exitosas, fallidas y tiempo de ejecución
* Contribuciones por persona
* Git timeline
* Contributors

# Rastreo de errores

Sentry